var num1 = 0.0;
var num2 = 0.0;
var result = 0.0;
var operador = "";
var tstring = "";

$(function() {

    $("#retr").on("click",
        function() {
            var texto = $('#texto').val();
            var cadena = "";
        
            for (var i = 0; i < texto.length-1; i++) {
                cadena += texto[i];
                
            }
            $('#texto').val(cadena); 
        }
    );

    $("#ce").on("click",
        function() {
            $('#texto').val("");
        }
    );

    $("#ce").on("click",
        function() {
            num1 = 0.0;
            num2 = 0.0;
            result = 0.0;
            operador = "";
            tstring = "";
            $('#texto').val("");
            $('#resul').val("");
        }
    );

    $("#ce").on("click",
        function() {
            num1 = 0.0;
            num2 = 0.0;
            result = 0.0;
            operador = "";
            tstring = "";
            $('#texto').val("");
            $('#resul').val("");
        }
    );

    $(".numeros").on("click",
        function() {

            $('#texto').val($('#texto').val() + $(this).attr("id")); 

        }
    );

    $(".op").on("click",
        function() {

        var oper = $(this).attr("id");

        if ($('#texto') === 0){
            alert("Introduce un numero");
            }
            else{
                tstring = $('#texto').val();
                num1 = parseFloat(tstring);
                switch (oper){
                    case "/":
                        operador = "/";
                        $('#texto').val('');
                        $('#resul').val(num1 + ' / ');
                        break;
                    case "raiz":
                        $('#texto').val(Math.sqrt(num1));
                        $('#resul').val(num1 + ' raiz= ' + Math.sqrt(num1));
                        break;
                    case "*":
                        operador = "*";
                        $('#texto').val('');
                        $('#resul').val(num1 + ' * ');
                        break;
                    case "%":
                        operador = "%";
                        $('#texto').val('');
                        $('#resul').val(num1 + ' % ');
                        break;
                    case "-":
                        operador = "-";
                        $('#texto').val('');
                        $('#resul').val(num1 + ' - ');
                        break;
                    case "1/x":
                        $('#texto').val(1 / num1);
                        $('#resul').val('1 / ' + num1 + ' = ' + 1 / num1);
                        break;
                    case "+":
                        operador = "+";
                        $('#texto').val('');
                        $('#resul').val(num1 + ' + ');
                        break;
                }
            }
        }
    );

    $("#punto").on("click",
    function() {
        if ($('#texto').val() !== ""){
    
            $('#texto').val($('#texto').val() + ".");
        
        }
    }
    );

    $("#masmenos").on("click",
    function() {
        if ($('#texto').val() !== ""){
    
            $('#texto').val("-" + $('#texto').val());
        
        }
    }
    );
    
    $("#resultado").on("click",
    function() {
        tstring = "";
        tstring = $('#texto').val();
        num2 = parseFloat(tstring);

        if (operador === "/"){
            $('#texto').val(eval(num1 / num2).toFixed(2));
            $('#resul').val($('#resul').val() + num2 + '= ' + eval(num1 / num2).toFixed(2));
            num1 = 0.0, num2 = 0.0;
        }
        else if (operador === "*"){
            $('#texto').val(eval(num1 * num2).toFixed(2));
            $('#resul').val($('#resul').val() + num2 + '= ' + eval(num1 * num2).toFixed(2));
            num1 = 0.0, num2 = 0.0;
        }
        else if (operador === "%"){
            $('#texto').val(eval(num1 % num2).toFixed(2));
            $('#resul').val($('#resul').val() + num2 + '= ' + eval(num1 % num2).toFixed(2));
            num1 = 0.0, num2 = 0.0;
        }
        else if (operador === "-"){
            $('#texto').val(eval(num1 - num2).toFixed(2));
            $('#resul').val($('#resul').val() + num2 + '= ' + eval(num1 - num2).toFixed(2));
            num1 = 0.0, num2 = 0.0;
        }
        else if (operador === "+"){
            $('#texto').val(eval(num1 + num2).toFixed(2));
            $('#resul').val($('#resul').val() + num2 + '= ' + eval(num1 + num2).toFixed(2));
            num1 = 0.0, num2 = 0.0;
        }
    }
    );

});

